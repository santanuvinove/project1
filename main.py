from flask import Flask, render_template,request,redirect,url_for
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
app=Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///santanu.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
class Student(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120), nullable=False)
    username = db.Column(db.String(80),  nullable=False)
    desc=db.Column(db.String(200), nullable=False)
    date_created= db.Column(db.DateTime, nullable=False,
        default=datetime.utcnow)
    
    def __repr__(self):
        return f' {self.title} - {self.username}'
@app.route("/",methods=["POST","GET"])
def home_display():
    if request.method == "POST":
        title=request.form["title"]
        username=request.form["username"]
        desc=request.form["desc"]
        stu=Student(title=title,username=username,desc=desc)
        # user=Student.query.filter_by(title="this is first project",email="samnatanu@mail.com",username="santanu",desc="plz check this is true or flase")
        db.session.add(stu)
        db.session.commit()
    allstudent=Student.query.all()
    return render_template("home.html",students=allstudent)
@app.route("/update/<int:id>",methods=["POST","GET"])
def update(id):
    if request.method == "POST":
        title=request.form['title']
        desc=request.form['desc']
        stud=Student.query.filter_by(id=id).first()
        stud.title=title
        stud.desc=desc
        db.session.add(stud)
        db.session.commit()
        return redirect("/")
    students=Student.query.filter_by(id=id).first()
    return render_template("update.html",students=students)
@app.route("/delete/<int:id>",methods=["POST","GET"])
def delete(id):
    stud=Student.query.filter_by(id=id).first()
    print(stud)
    db.session.delete(stud)
    db.session.commit()
    # return render_template("delete.html")
    # return redirect (url_for("home_display"))
    return redirect("/")

if __name__ == "__main__":
    app.run(debug=True)